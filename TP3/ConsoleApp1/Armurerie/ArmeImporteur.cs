﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ConsoleApp2
{
    class ArmeImporteur
    {
        private int longeurMinMot;
        private List<string> listeNoire;
        Dictionary<string, int> frequenceMots;

        ArmeImporteur(int longeurMinMot, List<String> listeNoire)
        {
            this.longeurMinMot = longeurMinMot;
            this.listeNoire = listeNoire;
        }

        public void CompteFrequenceMots(string fichier)
        {
            frequenceMots = File.ReadLines(fichier)
               .SelectMany(x => x.Split())
               .Where(x => Validation(x))
               .GroupBy(x => x)
               .ToDictionary(x => x.Key, x => x.Count());
        }

        public bool Validation(string mot)
        {
            if (mot.Length < longeurMinMot || listeNoire.Contains(mot))
            {
                return false;
            }
            return true;
        }

        public void CreeArmes()
        {
            foreach (KeyValuePair<string, int> mot in frequenceMots)
            {
                // Le nom de l'arme est le mot donné formatté 
                string nom = SpaceInvadersHelper.AppliqueFormatNom(mot.Key);
                // On génère les atributs de dégats selon la fréquence et la longeur des mots
                int degatsMin = mot.Key.Length * mot.Value;
                int degatsMax = (int) Math.Round(degatsMin * 1.5);
                // On génère un temps de chargement aléatoire entre 1 et 4
                int tempsRechargement = SpaceInvadersHelper.RandomInstance.Next(4) + 1; 

                // Création de l'arme
                Arme arme = new Arme(
                    nom,
                    GenereTypeAleatoire(),
                    degatsMin,
                    degatsMax,
                    tempsRechargement
                    );

                // On ajoute l'arme à la liste des armes disponibles dans l'Armurerie
                Armurerie.Instance.Armes.Add(arme);

            }
        }

        // Permet de récupérer un type alétoire parmis ceux existant
        private Type GenereTypeAleatoire()
        {
            Array valeurs = Type.GetValues(typeof(Type));
            Random rnd = SpaceInvadersHelper.RandomInstance;
            return (Type)valeurs.GetValue(rnd.Next(valeurs.Length));
        }
    }
}
