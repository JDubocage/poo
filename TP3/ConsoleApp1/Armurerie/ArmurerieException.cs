﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class ArmurerieException : Exception
    {
        public ArmurerieException() { }

        public ArmurerieException(string message): base(message) { }

        public ArmurerieException(string message, Exception inner): base(message, inner) { }
    }
}
