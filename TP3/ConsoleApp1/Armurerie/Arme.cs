﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    enum Type
    {
        Direct,
        Explosif,
        Guide
    }
    class Arme
    {
        public string Nom { get; }
        public Type Type { get; }
        public int DegatsMin { get; }
        public int DegatsMax { get; }
        public double TempsRechargement { get; set; }
        private double compteurRechargement;
        
        // propriété correspondant au dégats moyen de l'arme
        public int DegatsMoy
        {
            get => (DegatsMin + DegatsMax) / 2;
        }

        public Arme(string nom, Type type, int degatsMin, int degatsMax, double tempsRechargement)
        {
            this.Nom = nom;
            this.DegatsMin = degatsMin;
            this.DegatsMax = degatsMax;
            this.Type = type;
            this.TempsRechargement = tempsRechargement;
            // On arrondi la valeur des explosifs pour ne pas avoir des problèmes lors des décrémentations
            this.compteurRechargement = Type == Type.Explosif ? TempsRechargement * 2 : TempsRechargement;
        }

        public int Tire()
        {
            // On décrémente le compteur
            this.compteurRechargement--;

            if (compteurRechargement > 0)
            {
                // L'arme est en train de se recharger elle ne peut pas tirer => 0 dégâts
                return 0;
            }

            if (compteurRechargement < 0)
            {
                throw new Exception("La valeur du compteur de rechargement ne doit pas être négative");
            }


            switch (this.Type)
            {
                case Type.Direct:
                    // Si l’arme est de type direct alors elle a 1 chance sur 10 de rater sa cible (0 dégât)
                    compteurRechargement = TempsRechargement;
                    if (SpaceInvadersHelper.RandomInstance.Next(10) == 0)
                    {
                        return 0;
                    }
                    return SpaceInvadersHelper.RandomInstance.Next(DegatsMin, DegatsMax);
                case Type.Explosif:
                    // Une arme de type explosif multiplie le résultat et le temps de rechargement par 2, et a 1 chance sur 4 de rater
                    compteurRechargement = TempsRechargement * 2;
                    if (SpaceInvadersHelper.RandomInstance.Next(3) == 0)
                    {
                        return 0;
                    }
                    return SpaceInvadersHelper.RandomInstance.Next(DegatsMin, DegatsMax) * 2;
                case Type.Guide:
                    // Une arme de type guidée touche toujours, mais avec les dégâts minimums
                    compteurRechargement = TempsRechargement;
                    return this.DegatsMin;
                default:
                    compteurRechargement = TempsRechargement;
                    return SpaceInvadersHelper.RandomInstance.Next(DegatsMin, DegatsMax);
            }

        }

        public override string ToString()
        {
            return Nom + " : " + Type + "(" + DegatsMin + " - " + DegatsMax + " : Cooldown = " + TempsRechargement + ")" ;
        }
    }
}
