﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Joueur
    {
        private string nom;
        private string prenom;
        public string Pseudo { get; set; }
        public Vaisseau Vaisseau { get; }
        private string NomEntier 
        { 
            get => prenom + " " + nom;
        }

        public Joueur(string nom, string prenom, string pseudo)
        {
            this.nom = SpaceInvadersHelper.AppliqueFormatNom(nom);
            this.prenom = SpaceInvadersHelper.AppliqueFormatNom(prenom);
            this.Pseudo = pseudo;
            this.Vaisseau = new ViperMKII();
        }

        public override string ToString()
        {
            return Pseudo + "(" + NomEntier + ")";
        }

        // Permet de déterminer si deux joueurs sont égaux 
        public override bool Equals(object obj)
        {
            if (obj is Joueur)
            {
                Joueur j = (Joueur)obj;
                if (j.Pseudo.Equals(this.Pseudo))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
