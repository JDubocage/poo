﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp2
{
    class SpaceInvaders
    {
        private Joueur joueur;
        private List<Joueur> profilsJoueurs;
        private List<Vaisseau> ennemis;
        SpaceInvaders()
        {
            Init();
        }

        static void Main()
        {
            // Création d'une nouvelle partie de SpaceInvaders
            SpaceInvaders partie = new SpaceInvaders();

            Console.WriteLine("La partie commence !");

            // Tant que le vaisseau du joueur n’est pas détruit et qu’il reste des vaisseaux ennemis, on relance un tour
            while (partie.joueur.Vaisseau.PointsStructure > 0 && partie.ennemis.Count != 0)
            {
                Console.WriteLine("\nNouveau Tour");
                partie.LanceTour();
                // On retire de la partie tout les ennemis dont les points de structure sont égal à 0
                partie.ennemis.FindAll(ennemi => ennemi.PointsStructure == 0).ForEach(ennemi => {
                    Console.WriteLine("Le " + ennemi + " est éliminé !");
                    partie.ennemis.Remove(ennemi);
                });
                Thread.Sleep(3000);
            }

            Console.WriteLine("\nLa partie est terminée !");
            if (partie.ennemis.Count > 0)
            {
                Console.WriteLine("Les ennemis gagnent");
            }
            else Console.WriteLine("Le joueur gagne");
        }

        private void MenuNavigation()
        {
            Console.WriteLine("*************** Menu de Navigation ***************");
            Console.WriteLine("1. Créer un joueur");
            Console.WriteLine("2. Supprimer un joueur");
            Console.WriteLine("3. Choisir un joueur\n");

            // On demande son choix à l'utilisateur
            int choix = SpaceInvadersHelper.RecupereValeurUtilisateur(3);

            Console.WriteLine();

            switch (choix)
            {
                case 1:
                    CreeJoueur();
                    break;
                case 2:
                    SupprimeJoueur();
                    break;
                case 3:
                    SelectionneJoueur();
                    break;
            }

        }

        private void MenuVaisseau()
        {
            Console.WriteLine("*************** Menu du Vaisseau ***************");
            Console.WriteLine("1. Ajouter une arme");
            Console.WriteLine("2. Supprimer une arme");
            Console.WriteLine("3. Afficher les armes\n");

            Console.WriteLine();

            // On demande son choix à l'utilisateur
            int choix = SpaceInvadersHelper.RecupereValeurUtilisateur(3);

            switch (choix)
            {
                case 1:
                    AjouterArmeVaisseau();
                    break;
                case 2:
                    SupprimerArmeVaisseau();
                    break;
                case 3:
                    joueur.Vaisseau.AfficheArmes();
                    break;
            }
        }

        private void MenuArmurerie()
        {
            Console.WriteLine("*************** Menu Armurerie ***************");
            Console.WriteLine("1. Créer une arme");
            Console.WriteLine("2. Supprimer une arme");
            Console.WriteLine("3. Afficher les armes\n");
        }


        private void AjouterArmeVaisseau()
        {
            if (Armurerie.Instance.Armes.Count < 1)
            {
                Console.WriteLine("Il n'existe aucune arme dans l'armurerie");
                return;
            }

            // Affichage des armes de l'armurerie
            Armurerie.Instance.Afficher();

            // On demande à l'utilisateur quelle arme il veut ajouter à son vaisseau
            int choix = SpaceInvadersHelper.RecupereValeurUtilisateur(Armurerie.Instance.Armes.Count);
            Arme arme = Armurerie.Instance.Armes[choix - 1];

            joueur.Vaisseau.AjouterArme(arme);
            Console.WriteLine("Arme ajouée");
        }

        private void SupprimerArmeVaisseau()
        {
            if (joueur.Vaisseau.Armes.Count < 1) return;

            // Affichage des armes du vaisseau
            joueur.Vaisseau.AfficheArmes();

            // On demande à l'utilisateur quelle arme il veut supprimer
            int choix = SpaceInvadersHelper.RecupereValeurUtilisateur(joueur.Vaisseau.Armes.Count);
            Arme arme = Armurerie.Instance.Armes[choix - 1];

            joueur.Vaisseau.RetirerArme(arme);
            Console.WriteLine("Arme supprimée");
        }

        private void CreeJoueur()
        {
            Console.WriteLine("Veuillez entrer le nom du joueur : ");
            string nom = Console.ReadLine();
            Console.WriteLine("Veuillez entrer le prénom du joueur : ");
            string prenom = Console.ReadLine();
            Console.WriteLine("Veuillez entrer le pseudo du joueur : ");
            string pseudo = Console.ReadLine();

            // Création du joueur
            Joueur joueur = new Joueur(nom, prenom, pseudo);

            // Ajout du joueur aux profils
            profilsJoueurs.Add(joueur);
            Console.WriteLine("Joueur ajouté");
        }

        private void SupprimeJoueur()
        {
            if (profilsJoueurs.Count < 1)
            {
                Console.WriteLine("Il n'existe aucun joueur à supprimer");
                return;
            }
            
            Console.WriteLine("Voici la liste des profils existants : ");
            AfficheProfils();

            // On demande son choix à l'utilisateur
            int choix = SpaceInvadersHelper.RecupereValeurUtilisateur(profilsJoueurs.Count);

            // Supression du joueur de la liste des profils
            profilsJoueurs.RemoveAt(choix - 1);
            Console.WriteLine("Joueur supprimé");

        }

        private void AfficheProfils()
        {
            for (int i = 0; i < profilsJoueurs.Count; i++)
            {
                Console.WriteLine($"{i+1}. {profilsJoueurs[i]}");
            }
            Console.WriteLine();
        }

        private void SelectionneJoueur()
        {
            if (profilsJoueurs.Count < 1)
            {
                Console.WriteLine("Il n'existe aucun joueur à sélectionner");
                return;
            }

            Console.WriteLine("Voici la liste des profils existants : ");
            AfficheProfils();

            // On demande son choix à l'utilisateur
            int choix = SpaceInvadersHelper.RecupereValeurUtilisateur(profilsJoueurs.Count);

            // Supression du joueur de la liste des profils
            joueur = profilsJoueurs[choix - 1];
            Console.WriteLine("Joueur sélectionné");
        }

        // Initialise une nouvelle partie
        private void Init()
        {
            // Création du joueur
            joueur = new Joueur("Zablot", "Jonathan", "Kirby54");

            // Population de la liste d'ennemis
            ennemis = new List<Vaisseau>();
            ennemis.Add(new F18(joueur.Vaisseau));
            ennemis.Add(new BWings(joueur.Vaisseau));
            ennemis.Add(new Dart(joueur.Vaisseau));
            ennemis.Add(new Rocinante(joueur.Vaisseau));
            ennemis.Add(new Tardis(joueur.Vaisseau));
        }

        private void LanceTour()
        {
            DeclencheAptitudes();

            int chancesTir = 1;
            foreach (Vaisseau ennemi in ennemis)
            {
                Console.WriteLine("\nTour de " + ennemi.ToString());

                // Chaque début de tour les vaisseaux ayant perdu des points de bouclier en regagne maximum 2
                RechargeBouclier(ennemi);

                ennemi.AfficheInfos();
                // Tous les ennemis tirent sur le vaisseau du joueur
                ennemi.Attaque(joueur.Vaisseau);

                // Chance du joueur de pouvoir tirer
                if (chancesTir != -1 && SpaceInvadersHelper.RandomInstance.Next(chancesTir, ennemis.Count) == ennemis.Count - 1)
                {
                    // Le joueur tire sur un vaisseau non détruit au hasard dans la liste d’ennemis
                    Console.WriteLine("\nTour de " + joueur.Vaisseau);
                    joueur.Vaisseau.AfficheInfos();
                    int choixEnnemi = SpaceInvadersHelper.RandomInstance.Next(ennemis.Count);
                    joueur.Vaisseau.Attaque(ennemis[choixEnnemi]);
                    chancesTir = -1;
                }
                else chancesTir++;

                Thread.Sleep(1000);

            }
        }

        private void DeclencheAptitudes()
        {
            List<Vaisseau> listeEnnemisFige = new List<Vaisseau>();
            listeEnnemisFige.AddRange(ennemis);
            foreach (Vaisseau ennemi in listeEnnemisFige)
            {
                // Si l'ennemi possède une aptitude, il l'utilise
                if (ennemi is IAptitude)
                {
                    ((IAptitude)ennemi).Utilise(ennemis);
                }
            }
        }

        private void RechargeBouclier(Vaisseau vaisseau)
        {
            if (vaisseau.PointsBouclier < vaisseau.POINTS_BOUCLIER_MAX)
            {
                vaisseau.PointsBouclier += SpaceInvadersHelper.RandomInstance.Next(2);
            }
        }

    }
}
