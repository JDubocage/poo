﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    abstract class Vaisseau
    {
        public int POINTS_STRUCTURE_MAX { get; }
        public int POINTS_BOUCLIER_MAX { get;  }
        public int PointsStructure { get; set; }
        public int PointsBouclier { get; set; }

        public List<Arme> Armes { get; set; }

        public Vaisseau(int POINTS_STRUCTURE_MAX, int POINTS_BOUCLIER_MAX)
        {
            this.POINTS_STRUCTURE_MAX = POINTS_STRUCTURE_MAX;
            this.POINTS_BOUCLIER_MAX = POINTS_BOUCLIER_MAX;
            this.PointsStructure = POINTS_STRUCTURE_MAX;
            this.PointsBouclier = POINTS_BOUCLIER_MAX;
            this.Armes = new List<Arme>();
        }

        public abstract void Attaque(Vaisseau vaisseau);

        // Ajoute une arme à la liste des armes du vaisseau (maximum de 3)
        public void AjouterArme(Arme arme)
        {
            if (!Armurerie.Instance.Armes.Contains(arme))
            {
                throw new ArmurerieException("L'arme ajoutée au vaisseau n'existe pas dans l'armurerie");
            }

            if (Armes.Count >= 3)
            {
                throw new VaisseauException("Nombre d'armes maximum atteint");
            }

            Armes.Add(arme);

        }

        // Retire une arme donnée de la liste des armes du vaisseau
        public void RetirerArme(Arme arme)
        {
            if (!Armes.Contains(arme))
            {
                throw new VaisseauException("Le vaisseau ne possède pas cette arme");
            }

            Armes.Remove(arme);
        }

        // Affiche la liste des armes du vaisseau
        public void AfficheArmes()
        {
            if (Armes.Count == 0)
            {
                Console.WriteLine("Le vaisseau ne contient pas d'armes");
                return;
            }

            Console.WriteLine("Armes du vaisseau : ");
            int i = 1;
            foreach (Arme a in Armes)
            {
                Console.WriteLine($"{i}. {a.ToString()}");
            }
        }

        // Calcule puis renvoie la somme des dégats moyens de la liste des armes du vaisseau
        public int CalculeDegatsMoyen()
        {
            int degats = 0;
            foreach(Arme a in Armes)
            {
                degats += a.DegatsMoy;
            }
            return degats;
        }

        public void RepartitDegats(int degats)
        {
            // Si il n'y a plus de bouclier, c'est la structure qui prends les dégats
            if (PointsBouclier == 0)
            {
                if (degats > PointsStructure)
                {
                    PointsStructure = 0;
                }
                else PointsStructure -= degats;
            }

            // Si les degats sont supérieurs au points de bouclier, on repartit les dégats entre le bouclier et la structure
            if (degats > PointsBouclier)
            {
                PointsBouclier = 0;
                if (degats - PointsBouclier > PointsStructure)
                {
                    PointsStructure = 0;
                }
                else PointsStructure -= (degats - PointsBouclier);
            }

            // Sinon c'est juste le bouclier qui prends les dégâts
            PointsBouclier -= degats;
        }

        // Affiche les différentes informations disponibles sur le vaisseau
        public void AfficheInfos()
        {
            Console.WriteLine("Points de structure : " + PointsStructure);
            Console.WriteLine("Points de bouclier : " + PointsBouclier);
            AfficheArmes();
            Console.WriteLine("Degats moyen : " + CalculeDegatsMoyen());
        }
    }
}
