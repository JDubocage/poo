﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    interface IAptitude
    {
        public void Utilise(List<Vaisseau> vaisseaux);
    }
}
