﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class BWings : Vaisseau
    {
        new const int POINTS_STRUCTURE_MAX = 30;
        new const int POINTS_BOUCLIER_MAX = 0;
        private Vaisseau vaisseauJoueur;

        public BWings(Vaisseau vaisseauJoueur) : base(POINTS_STRUCTURE_MAX, POINTS_BOUCLIER_MAX)
        {
            // Arme par défaut : Hammer de type explosif avec des dégâts (1-8) et un temps de recharge de 1,5
            Arme armeParDefaut = Armurerie.Instance.RecupereArmeParNom("Hammer");
            this.AjouterArme(armeParDefaut);
            this.vaisseauJoueur = vaisseauJoueur;
        }

        public new void AjouterArme(Arme arme)
        {
            // Un B-Wings utilise toutes les armes de type explosif avec un compteur de rechargement à 1 par tour
            if (arme.Type == Type.Explosif)
            {
                arme.TempsRechargement = 1;
            }
            base.AjouterArme(arme);
        }

        public override void Attaque(Vaisseau vaisseau)
        {
            foreach (Arme arme in Armes)
            {
                int degats = arme.Tire();
                if (degats > 0)
                {
                    vaisseau.RepartitDegats(degats);
                    Console.WriteLine("Le " + this + "tire avec son " + arme.Nom + " et occasionne " + degats + " degats sur le " + vaisseau);
                }
                
            }
        }

        public override string ToString()
        {
            return "B-Wings";
        }
    }
}
