﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class F18 : Vaisseau, IAptitude
    {
        new const int POINTS_STRUCTURE_MAX = 15;
        new const int POINTS_BOUCLIER_MAX = 0;
        private Vaisseau vaisseauJoueur;
        public F18(Vaisseau vaisseauJoueur) : base(POINTS_STRUCTURE_MAX, POINTS_BOUCLIER_MAX)
        {
            this.vaisseauJoueur = vaisseauJoueur;
        }

        // Le F-18 ne possède pas d'armes
        public override void Attaque(Vaisseau vaisseau) {}

        /* Si le F-18 est le premier vaisseau encore en vie de la liste d'ennemis, 
         * il explose au contact du vaisseau du joueur et lui inflige 10 points de degats */
        public void Utilise(List<Vaisseau> vaisseaux)
        {

            if (vaisseaux[0].Equals(this))
            {
                Console.WriteLine("Le F-18 utilise son aptitude");
                this.PointsStructure = 0;
                vaisseauJoueur.RepartitDegats(10);
                Console.WriteLine("Il explose et occasionne 10 dégats au joueur");
            }
        }

        public override string ToString()
        {
            return "F-18";
        }
    }
}
