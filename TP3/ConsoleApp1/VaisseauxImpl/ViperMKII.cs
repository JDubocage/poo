﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class ViperMKII : Vaisseau
    {
        new const int POINTS_STRUCTURE_MAX = 10;
        new const int POINTS_BOUCLIER_MAX = 15;

        public ViperMKII() : base(POINTS_STRUCTURE_MAX, POINTS_BOUCLIER_MAX)
        {
            this.AjouterArme(Armurerie.Instance.RecupereArmeParNom("Mitrailleuse"));
            this.AjouterArme(Armurerie.Instance.RecupereArmeParNom("EMG"));
            this.AjouterArme(Armurerie.Instance.RecupereArmeParNom("Missile"));
        }
        public override void Attaque(Vaisseau vaisseau)
        {
            foreach (Arme arme in Armes)
            {
                int degats = arme.Tire();
                vaisseau.RepartitDegats(degats);
                if (degats > 0)
                {
                    vaisseau.RepartitDegats(degats);
                    Console.WriteLine("Le " + this + " tire avec son " + arme.Nom + " et occasionne " + degats + " degats sur le " + vaisseau);
                }
            }
        }

        public override string ToString()
        {
            return "ViperMKII (Joueur)";
        }
    }
}
