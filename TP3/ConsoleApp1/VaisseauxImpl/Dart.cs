﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Dart : Vaisseau
    {
        new const int POINTS_STRUCTURE_MAX = 10;
        new const int POINTS_BOUCLIER_MAX = 3;
        private Vaisseau vaisseauJoueur;

        public Dart(Vaisseau vaisseauJoueur) : base(POINTS_STRUCTURE_MAX, POINTS_BOUCLIER_MAX)
        {
            // Arme par défaut : Laser de type direct avec des dégâts (2-3) et un temps de recharge de 1
            Arme armeParDefaut = Armurerie.Instance.RecupereArmeParNom("Laser");
            this.AjouterArme(armeParDefaut);
            this.vaisseauJoueur = vaisseauJoueur;
        }

        public new void AjouterArme(Arme arme)
        {
            // Un Dart utilise toutes les armes de type Direct avec un compteur de rechargement à 1 par tour
            if (arme.Type == Type.Direct)
            {
                arme.TempsRechargement = 1;
            }
            base.AjouterArme(arme);
        }

        public override void Attaque(Vaisseau vaisseau)
        {
            foreach (Arme arme in Armes)
            {
                int degats = arme.Tire();
                if (degats > 0)
                {
                    vaisseau.RepartitDegats(degats);
                    Console.WriteLine("Le " + this + "tire avec son " + arme.Nom + " et occasionne " + degats + " degats sur le " + vaisseau);
                }
            }
        }

        public override string ToString()
        {
            return "Dart";
        }
    }
}
