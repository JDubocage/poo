﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1
{
    class SpaceInvaders
    {
        private Joueur joueur;
        private List<Vaisseau> ennemis;
        SpaceInvaders()
        {
            Init();
        }

        static void Main()
        {
            // Création d'une nouvelle partie de SpaceInvaders
            SpaceInvaders partie = new SpaceInvaders();

            Console.WriteLine("La partie commence !");

            // Tant que le vaisseau du joueur n’est pas détruit et qu’il reste des vaisseaux ennemis, on relance un tour
            while (partie.joueur.Vaisseau.PointsStructure > 0 && partie.ennemis.Count != 0)
            {
                Console.WriteLine("\nNouveau Tour");
                partie.LanceTour();
                // On retire de la partie tout les ennemis dont les points de structure sont égal à 0
                partie.ennemis.FindAll(ennemi => ennemi.PointsStructure == 0).ForEach(ennemi => {
                    Console.WriteLine("Le " + ennemi + " est éliminé !");
                    partie.ennemis.Remove(ennemi);
                });
                Thread.Sleep(3000);
            }

            Console.WriteLine("\nLa partie est terminée !");
            if (partie.ennemis.Count > 0)
            {
                Console.WriteLine("Les ennemis gagnent");
            }
            else Console.WriteLine("Le joueur gagne");
        }

        // Initialise une nouvelle partie
        private void Init()
        {
            // Création du joueur
            joueur = new Joueur("Zablot", "Jonathan", "Kirby54");

            // Population de la liste d'ennemis
            ennemis = new List<Vaisseau>();
            ennemis.Add(new F18(joueur.Vaisseau));
            ennemis.Add(new BWings(joueur.Vaisseau));
            ennemis.Add(new Dart(joueur.Vaisseau));
            ennemis.Add(new Rocinante(joueur.Vaisseau));
            ennemis.Add(new Tardis(joueur.Vaisseau));
        }

        private void LanceTour()
        {
            DeclencheAptitudes();

            int chancesTir = 1;
            foreach (Vaisseau ennemi in ennemis)
            {
                Console.WriteLine("\nTour de " + ennemi.ToString());

                // Chaque début de tour les vaisseaux ayant perdu des points de bouclier en regagne maximum 2
                RechargeBouclier(ennemi);

                ennemi.AfficheInfos();
                // Tous les ennemis tirent sur le vaisseau du joueur
                ennemi.Attaque(joueur.Vaisseau);

                // Chance du joueur de pouvoir tirer
                if (chancesTir != -1 && SpaceInvadersHelper.RandomInstance.Next(chancesTir, ennemis.Count) == ennemis.Count - 1)
                {
                    // Le joueur tire sur un vaisseau non détruit au hasard dans la liste d’ennemis
                    Console.WriteLine("\nTour de " + joueur.Vaisseau);
                    joueur.Vaisseau.AfficheInfos();
                    int choixEnnemi = SpaceInvadersHelper.RandomInstance.Next(ennemis.Count);
                    joueur.Vaisseau.Attaque(ennemis[choixEnnemi]);
                    chancesTir = -1;
                }
                else chancesTir++;

                Thread.Sleep(1000);

            }
        }

        private void DeclencheAptitudes()
        {
            List<Vaisseau> listeEnnemisFige = new List<Vaisseau>();
            listeEnnemisFige.AddRange(ennemis);
            foreach (Vaisseau ennemi in listeEnnemisFige)
            {
                // Si l'ennemi possède une aptitude, il l'utilise
                if (ennemi is IAptitude)
                {
                    ((IAptitude)ennemi).Utilise(ennemis);
                }
            }
        }

        private void RechargeBouclier(Vaisseau vaisseau)
        {
            if (vaisseau.PointsBouclier < vaisseau.POINTS_BOUCLIER_MAX)
            {
                vaisseau.PointsBouclier += SpaceInvadersHelper.RandomInstance.Next(2);
            }
        }

    }
}
