﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Armurerie
    {
        public List<Arme> Armes { get; set; }

        public Armurerie()
        {
            Init();
        }

        private void Init()
        {
            Armes = new List<Arme>();
            Armes.Add(new Arme("Lance-Pierre", Type.Direct, 1, 5));
            Armes.Add(new Arme("Bombes", Type.Explosif, 50, 85));
            Armes.Add(new Arme("Missiles", Type.Guide, 70, 100));
        }

        // Affiche la liste des armes disponibles dans l'armurerie
        public void Afficher()
        {
            Console.WriteLine("Armes disponibles dans l'armurerie : ");
            foreach(Arme a in Armes)
            {
                Console.WriteLine(a.ToString());
            }
        }
    }
}
