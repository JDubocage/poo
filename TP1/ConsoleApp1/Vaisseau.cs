﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Vaisseau
    {
        const int POINTS_STRUCTURE_MAX = 150;
        const int POINTS_BOUCLIER_MAX = 150;
        public int PointsStructure { get; set; }
        public int PointsBouclier { get; set; }

        private List<Arme> armes;

        public Vaisseau()
        {
            this.PointsStructure = POINTS_STRUCTURE_MAX;
            this.PointsBouclier = POINTS_BOUCLIER_MAX;
            this.armes = new List<Arme>();
        }

        // Ajoute une arme à la liste des armes du vaisseau (maximum de 3)
        public void ajouterArme(Armurerie armurerie, Arme arme)
        {
            if (!armurerie.Armes.Contains(arme))
            {
                throw new ArmurerieException("L'arme ajoutée au vaisseau n'existe pas dans l'armurerie");
            }

            if (armes.Count >= 3)
            {
                throw new VaisseauException("Nombre d'armes maximum atteint");
            }

            armes.Add(arme);

        }

        // Retire une arme donnée de la liste des armes du vaisseau
        public void retirerArme(Arme arme)
        {
            if (!armes.Contains(arme))
            {
                throw new VaisseauException("Le vaisseau ne possède pas cette arme");
            }

            armes.Remove(arme);
        }

        // Affiche la liste des armes du vaisseau
        public void AfficheArmes()
        {
            if (armes.Count == 0)
            {
                Console.WriteLine("Le vaisseau ne contient pas d'armes");
                return;
            }

            Console.WriteLine("Armes du vaisseau : ");
            foreach (Arme a in armes)
            {
                Console.WriteLine(a.ToString());
            }
        }

        // Calcule puis renvoie la somme des dégats moyens de la liste des armes du vaisseau
        public int CalculeDegatsMoyen()
        {
            int degats = 0;
            foreach(Arme a in armes)
            {
                degats += a.DegatsMoy;
            }
            return degats;
        }

        // Affiche les différentes informations disponibles sur le vaisseau
        public void AfficheInfos()
        {
            Console.WriteLine("Points de structure : " + PointsStructure);
            Console.WriteLine("Points de bouclier : " + PointsBouclier);
            AfficheArmes();
            Console.WriteLine("Degats moyen : " + CalculeDegatsMoyen());
        }
    }
}
