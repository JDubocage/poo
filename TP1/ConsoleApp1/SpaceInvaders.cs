﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class SpaceInvaders
    {
        private List<Joueur> joueurs;
        private Armurerie armurerie;
        SpaceInvaders()
        {
            armurerie = new Armurerie();
            joueurs = new List<Joueur>();
            Init();
        }

        static void Main()
        {
            // Création d'une nouvelle partie de SpaceInvaders
            SpaceInvaders partie = new SpaceInvaders();

            // Affichage des armes disponibles dans l'armurerie
            partie.armurerie.Afficher();
            Console.WriteLine();

            // Affichage des infos sur les vaisseaux de chaque joueur
            foreach (Joueur j in partie.joueurs)
            {
                Console.WriteLine("Vaisseau de " + j.ToString());
                j.VaisseauJoueur.AfficheInfos();
                Console.WriteLine();
            }
        }

        // Initialise une nouvelle partie (création des joueurs et distribution des armes)
        private void Init()
        {
            joueurs.Add(new Joueur("Metz", "Florian", "Flaybe"));
            joueurs.Add(new Joueur("Eber", "Fanny", "Pandabrutie"));
            joueurs.Add(new Joueur("Zablot", "Jonathan", "Kirby54"));

            var rand = new Random();
            // On ajoute une arme aléatoire aux joueurs
            foreach (Joueur j in joueurs)
            {
                Arme arme = armurerie.Armes[rand.Next(armurerie.Armes.Count)];
                j.VaisseauJoueur.ajouterArme(armurerie, arme);
            }
        }

    }
}
