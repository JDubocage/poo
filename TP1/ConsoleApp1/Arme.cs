﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    enum Type
    {
        Direct,
        Explosif,
        Guide
    }
    class Arme
    {
        private string nom;
        private Type type;
        private int degatsMin;
        private int degatsMax;
        
        // propriété correspondant au dégats moyen de l'arme
        public int DegatsMoy
        {
            get => (degatsMin + degatsMax) / 2;
        }

        public Arme(string nom, Type type, int degatsMin, int degatsMax)
        {
            this.nom = nom;
            this.degatsMin = degatsMin;
            this.degatsMax = degatsMax;
            this.type = type;
        }

        public override string ToString()
        {
            return nom + " : " + type + "(" + degatsMin + " - " + degatsMax + ")";
        }
    }
}
