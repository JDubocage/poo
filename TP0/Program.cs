﻿using System;
using System.Threading;
using System.Linq;

public class Program
{
    public static void Main()
    {
        bool quitter = true;

        do
        {
            Console.WriteLine("Bienvenue sur le TP !\n");

            Console.WriteLine("Donne moi ton nom");
            string nom = ObtientChaineValide();

            Console.WriteLine("Donne moi ton prenom");
            string prenom = ObtientChaineValide();

            Console.WriteLine("Bienvenue " + NomComplet(prenom, nom) + " !\n");

            Console.WriteLine("Donne moi ta taille (cm)");
            int taille = (int)ObtientNombreValide();

            Console.WriteLine("Donne moi ton poids (kg)");
            float poids = ObtientNombreValide();

            Console.WriteLine("Donne moi ton age");
            int age = (int)ObtientNombreValide();

            CheckAge(age);

            float imc = CalculeIMC(poids, taille);
            Console.WriteLine("IMC : " + imc.ToString("0.0"));
            CommenteIMC(imc);

            Console.WriteLine();

            EstimeCheveux();

            AfficheFinProgramme();
            int choix;

            do
            {
                Console.WriteLine("Veuillez entrer un nombre entre 1 et 4");
                choix = (int)ObtientNombreValide();
            }
            while (choix > 4);

            Console.WriteLine();

            switch (choix)
            {
                case 1:
                    QuitterProgramme();
                    return;
                case 2:
                    quitter = false;
                    break;
                case 3:
                    for (int i = 1; i <= 10; i++)
                    {
                        Console.WriteLine(i);
                        Thread.Sleep(1000);
                    }
                    QuitterProgramme();
                    return;
                case 4:
                    Console.WriteLine("Tata Jacqueline vous a bloqué et ne souhaite visiblement pas vous addresser la parole \nVeuillez RACCROCHER");
                    QuitterProgramme();
                    return;
            }
        }
        while (quitter == false);

    }

    static void CheckAge(int age)
    {
        if (age < 18)
        {
            Console.WriteLine(age + " ?? Vous êtes un NABOT");
        }
    }

    static string NomComplet(string prenom, string nom)
    {
        return prenom + " " + nom.ToUpper();
    }

    static float CalculeIMC(float poids, int taille)
    {
        float tailleMetre = taille / 100.0f;
        return poids / (tailleMetre * tailleMetre);
    }

    static void CommenteIMC(float imc)
    {
        const string COMMENTAIRE_ANOREXIE = "Attention à l’anorexie !";
        const string COMMENTAIRE_MAIGRICHON = "Vous êtes un peu maigrichon !";
        const string COMMENTAIRE_NORMALE = "Vous êtes de corpulence normale !";
        const string COMMENTAIRE_SURPOIDS = "Vous êtes en surpoids !";
        const string COMMENTAIRE_SEVERE = "Obésité sévère !";
        const string COMMENTAIRE_MORBIDE = "Obésité morbide !";

        if (imc < 16.5)
        {
            Console.WriteLine(COMMENTAIRE_ANOREXIE);
        }
        else if (imc >= 16.5 && imc < 18.5)
        {
            Console.WriteLine(COMMENTAIRE_MAIGRICHON);
        }
        else if (imc >= 18.5 && imc < 25)
        {
            Console.WriteLine(COMMENTAIRE_NORMALE);
        }
        else if (imc >= 25 && imc < 30)
        {
            Console.WriteLine(COMMENTAIRE_SURPOIDS);
        }
        else if (imc >= 35 && imc < 40)
        {
            Console.WriteLine(COMMENTAIRE_SEVERE);
        }
        else if (imc >= 40)
        {
            Console.WriteLine(COMMENTAIRE_MORBIDE);
        }

    }

    static void EstimeCheveux()
    {
        bool trouve = false;

        do
        {
            Console.WriteLine("Estimez le nombre de cheveux sur votre crâne");
            string estimationChaine = Console.ReadLine();
            int estimation;

            if (int.TryParse(estimationChaine, out estimation))
            {
                if (estimation >= 100000 && estimation < 150000)
                {
                    Console.WriteLine("Bravo !");
                    trouve = true;
                }
                else
                {
                    Console.WriteLine("Estimation incorrecte ! ");
                }
            }
            else
            {
                Console.WriteLine("Veuillez entrer un nombre ! ");
            }

        }
        while (trouve == false);
    }

    static string ObtientChaineValide()
    {
        bool valide = false;
        string chaine;

        do
        {
            chaine = Console.ReadLine();

            if (chaine.Any(char.IsDigit))
            {
                Console.WriteLine("Cette chaine ne doit pas contenir de nombre !");
            }
            else
            {
                valide = true;
            }
        }
        while (valide == false);

        return chaine;

    }

    static float ObtientNombreValide()
    {
        bool valide = false;
        float nombre;

        do
        {
            string chaine = Console.ReadLine();

            if (float.TryParse(chaine, out nombre))
            {
                if (nombre > 0)
                {
                    valide = true;
                }
            }
            else
            {
                Console.WriteLine("Veuillez entrer un nombre valide ! ");
            }
        }
        while (valide == false);

        return nombre;
    }

    static void AfficheFinProgramme()
    {
        const string choix1 = "1 - Quitter le programme";
        const string choix2 = "2 - Recommencer le programme";
        const string choix3 = "3 - Compter jusqu’à 10";
        const string choix4 = "4 - Téléphoner à Tata Jacqueline";

        Console.WriteLine("\nFin du programme, veuillez faire un choix :");
        Console.WriteLine(choix1);
        Console.WriteLine(choix2);
        Console.WriteLine(choix3);
        Console.WriteLine(choix4);
        Console.WriteLine();
    }

    static void QuitterProgramme()
    {
        Console.WriteLine("\nAu revoir");
        Thread.Sleep(3000);
    }


}