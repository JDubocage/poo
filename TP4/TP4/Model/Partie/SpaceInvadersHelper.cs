﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp2
{
    class SpaceInvadersHelper
    {

        // Renvoie le format Nom/Prénom pour une chaîne donnée
        // (Premier charactère en majuscule puis le reste en minuscule) 
        public static string AppliqueFormatNom(string chaine)
        {
            return Char.ToUpper(chaine[0]) + chaine.Substring(1).ToLower();
        }

        // Renvoie le nom respectant le format pour une arme donnée
        // (Pas de ponctuation / Case)
        public static string AppliqueFormatArme(string chaine)
        {
            return new string(chaine.Where(c => !char.IsPunctuation(c)).ToArray()).ToLower();
        }

        // Patron singleton pour l'aléatoire utilisé pendant la partie
        private static readonly Lazy<Random> lazy = new Lazy<Random>(() => new Random());
        public static Random RandomInstance
        {
            get => lazy.Value;
        }

        public static int RecupereValeurUtilisateur(int borneSup)
        {
            int choix = 0;
            do
            {
                Console.WriteLine("Entrez votre choix : ");
                try
                {
                    choix = int.Parse(Console.ReadLine());
                    if (choix < 1 || choix > borneSup)
                    {
                        Console.WriteLine($"Le choix doit être compris entre 1 et {borneSup}");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Veuillez entrer un nombre");
                }
            }
            while (choix < 1 || choix > borneSup);

            return choix;
        }
    }
}
