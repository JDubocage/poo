﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class VaisseauException : Exception
    {
        public VaisseauException() { }

        public VaisseauException (string message): base(message) { }

        public VaisseauException (string message, Exception inner): base(message, inner) { }
    }
}
