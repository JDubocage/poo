﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Rocinante : Vaisseau
    {
        new const int POINTS_STRUCTURE_MAX = 3;
        new const int POINTS_BOUCLIER_MAX = 5;
        private Vaisseau vaisseauJoueur;
        public Rocinante(Vaisseau vaisseauJoueur) : base(POINTS_STRUCTURE_MAX, POINTS_BOUCLIER_MAX) 
        {
            Arme armeParDefaut = Armurerie.Instance.RecupereArmeParNom("Torpille");
            this.AjouterArme(armeParDefaut);
            this.vaisseauJoueur = vaisseauJoueur;
        }
        public override void Attaque(Vaisseau vaisseau)
        {
            foreach (Arme arme in Armes)
            {
                int degats = arme.Tire();
                if (degats > 0)
                {
                    vaisseau.RepartitDegats(degats);
                    Console.WriteLine("Le " + this + "tire avec son " + arme.Nom + " et occasionne " + degats + " degats sur le " + vaisseau);
                }
            }
        }

        public override string ToString()
        {
            return "Rocinante";
        }
    }
}
