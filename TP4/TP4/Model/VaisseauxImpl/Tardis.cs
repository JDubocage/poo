﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Tardis : Vaisseau, IAptitude
    {
        new const int POINTS_STRUCTURE_MAX = 1;
        new const int POINTS_BOUCLIER_MAX = 0;
        private Vaisseau vaisseauJoueur;
        public Tardis(Vaisseau vaisseauJoueur) : base(POINTS_STRUCTURE_MAX, POINTS_BOUCLIER_MAX)
        {

        }

        // Le tardis ne possède pas d'armes
        public override void Attaque(Vaisseau vaisseau) {}

        // Déplace un vaisseau au hasard et le met à un autre endroit dans la liste, toujours au hasard
        public void Utilise(List<Vaisseau> vaisseaux)
        {
            Console.WriteLine("Le Tardis utilise son aptitude");

            int choixVaisseau = SpaceInvadersHelper.RandomInstance.Next(vaisseaux.Count);
            int placeVaisseau = SpaceInvadersHelper.RandomInstance.Next(vaisseaux.Count);

            Console.WriteLine("Il change de place le " + vaisseaux[choixVaisseau] + " avec le " + vaisseaux[placeVaisseau]);

           Vaisseau temp = vaisseaux[placeVaisseau];
            vaisseaux[placeVaisseau] = vaisseaux[choixVaisseau];
            vaisseaux[choixVaisseau] = temp;
        }

        public override string ToString()
        {
            return "Tardis";
        }
    }
}
