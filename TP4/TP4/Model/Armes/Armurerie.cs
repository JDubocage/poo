﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp2
{
    class Armurerie
    {
        #region Patern singleton
        private static readonly Lazy<Armurerie> lazy = new Lazy<Armurerie>(() => new Armurerie());
        public static Armurerie Instance
        {
            get => lazy.Value;
        }
        #endregion Patern singleton
        public List<Arme> Armes { get; set; }
        public Armurerie()
        {
            //Init();
            Armes = new List<Arme>();
        }
        private void Init()
        {
            // On créer ici la liste des armes disponibles 
            Armes.Add(new Arme("Laser", Type.Direct, 2, 3, 1));
            Armes.Add(new Arme("Hammer", Type.Explosif, 1, 8, 1.5));
            Armes.Add(new Arme("Torpille", Type.Guide, 3, 3, 2));
            Armes.Add(new Arme("Mitrailleuse", Type.Direct, 2, 3, 1));
            Armes.Add(new Arme("EMG", Type.Explosif, 1, 7, 1.5));
            Armes.Add(new Arme("Missile", Type.Guide, 4, 100, 4));
        }

        // Affiche la liste des armes disponibles dans l'armurerie
        public void Afficher()
        {
            Console.WriteLine("Armes disponibles dans l'armurerie : ");
            int i = 1;
            foreach(Arme a in Armes)
            {
                Console.WriteLine($"{i}. {a.ToString()}");
            }
        }

        public Arme RecupereArmeParNom(string nom)
        {
            Arme arme = this.Armes.FirstOrDefault(arme => arme.Nom == nom);

            if (arme == null)
            {
                throw new Exception("L'arme n'a pas été trouvée");
            }

            return arme;
        }

        public List<Arme> RecupereCinqMeilleuresArmesDegatsMoy()
        {
            return Armes.OrderByDescending(x => x.DegatsMoy).Take(5).ToList();
        }

        public List<Arme> RecupereCinqMeilleuresArmesDegatsMin()
        {
            return Armes.OrderByDescending(x => x.DegatsMin).Take(5).ToList();
        }

    }
}
