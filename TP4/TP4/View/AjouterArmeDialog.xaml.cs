﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TP4.ViewModel;

namespace TP4.View
{
    /// <summary>
    /// Logique d'interaction pour AjouterArmeDialog.xaml
    /// </summary>
    public partial class AjouterArmeDialog : Window
    {

        public AjouterArmeDialog(SpaceInvadersViewModel viewModel)
        {
            InitializeComponent();
            listViewArmes.ItemsSource = viewModel.ArmesArmurerie;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
