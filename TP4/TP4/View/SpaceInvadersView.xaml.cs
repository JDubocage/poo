﻿using ConsoleApp2;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TP4.ViewModel;

namespace TP4.View
{
    /// <summary>
    /// Logique d'interaction pour SpaceInvadersView.xaml
    /// </summary>
    public partial class SpaceInvadersView : Window
    {
        SpaceInvadersViewModel viewModel;
        public SpaceInvadersView()
        {
            InitializeComponent();
            viewModel = DataContext as SpaceInvadersViewModel;
        }

        private void buttonImport_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                viewModel.ImporterArme(openFileDialog.FileName);
            }
        }

        private void buttonAjouterJoueur_Click(object sender, RoutedEventArgs e)
        {
            JoueurInfosDialog dialog = new JoueurInfosDialog
            {
                Owner = this,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            if (dialog.ShowDialog() == true)
            {
                viewModel.CreerJoueur(dialog.Nom, dialog.Prenom, dialog.Pseudo);
            }
        }

        private void comboBoxJoueurs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxJoueurs.SelectedItem is Joueur)
            {
                viewModel?.ActualiserImage();
            }
        }


        private void buttonSupprimerJoueur_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.JoueurSelectionne != null)
                viewModel.SupprimerJoueurSelectionne();
        }

        private void buttonChangerImgVaisseau_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.JoueurSelectionne != null)
            {
                Microsoft.Win32.OpenFileDialog openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog() == true)
                {
                    viewModel.ChangerImageJoueurSelectionne(openFileDialog.FileName);
                }
            }
        }

        private void buttonAjouterArme_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.JoueurSelectionne != null)
            {
                AjouterArmeDialog dialog = new AjouterArmeDialog(viewModel)
                {
                    Owner = this,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner
                };
                if (dialog.ShowDialog() == true)
                {
                    Arme arme = dialog.listViewArmes.SelectedItem as Arme;
                    viewModel.AjouterArmeJoueurSelectionne(arme);
                }
            }
        }

        private void buttonSupprimerArme_Click(object sender, RoutedEventArgs e)
        {
            if (viewModel.JoueurSelectionne != null)
            {
                if (listViewArmesVaisseau.SelectedItem is Arme)
                {
                    Arme arme = listViewArmesVaisseau.SelectedItem as Arme;
                    viewModel.SupprimerArmeJoueurSelectionne(arme);
                }
            }
        }
    }
}
