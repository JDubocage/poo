﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TP4.View
{
    /// <summary>
    /// Interaction logic for JoueurInfosDialog.xaml
    /// </summary>
    public partial class JoueurInfosDialog : Window
    {
        public JoueurInfosDialog()
        {
            InitializeComponent();
        }

        public string Nom
        {
            get => nomTextBox.Text;
        }

        public string Prenom
        {
            get => prenomTextBox.Text;
        }

        public string Pseudo
        {
            get => pseudoTextBox.Text;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
