﻿using ConsoleApp2;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace TP4.ViewModel
{
    public class SpaceInvadersViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ObservableCollection<Arme> ArmesArmurerie
        {
            get => armesArmurerie;
            private set
            {
                armesArmurerie = value;
                OnPropertyChanged("ArmesArmurerie");
            }
        }
        private ObservableCollection<Arme> armesArmurerie;

        public ObservableCollection<Joueur> Profils
        {
            get => profils;
            private set
            {
                profils = value;
                OnPropertyChanged("Profils");
            }
        }
        private ObservableCollection<Joueur> profils;

        public Joueur JoueurSelectionne
        {
            get => joueurSelectionne;
            set
            {
                joueurSelectionne = value;
                OnPropertyChanged();
            }
        }
        private Joueur joueurSelectionne;

        public string ImageVaisseau
        {
            get => imageVaisseau;
            set
            {
                imageVaisseau = value;
                OnPropertyChanged();
            }
        }
        private string imageVaisseau;

        public SpaceInvadersViewModel()
        {
            Profils = new ObservableCollection<Joueur>();
        }

        public void ImporterArme(string fichier)
        {
            int longeurMinMot = 5;
            List<string> listeNoire = new List<string>();
            ArmeImporteur importeur = new ArmeImporteur(longeurMinMot, listeNoire, Armurerie.Instance);
            importeur.CreeArmes(fichier);
            ArmesArmurerie = new ObservableCollection<Arme>(Armurerie.Instance.Armes);
        }

        public void CreerJoueur(string nom, string prenom, string pseudo)
        {
            Joueur joueur = new Joueur(nom, prenom, pseudo);
            Profils.Add(joueur);
        }

        public void SupprimerJoueurSelectionne()
        {
            Profils.Remove(JoueurSelectionne);
            ImageVaisseau = null;
        }

        public void ActualiserImage()
        {
            ImageVaisseau = JoueurSelectionne.Vaisseau.CheminImage;
        }

        public void ChangerImageJoueurSelectionne(string cheminFichier)
        {
            joueurSelectionne.Vaisseau.CheminImage = cheminFichier;
            ActualiserImage();
        }

        public void AjouterArmeJoueurSelectionne(Arme arme)
        {
            joueurSelectionne?.Vaisseau.AjouterArme(arme);
        }

        public void SupprimerArmeJoueurSelectionne(Arme arme)
        {
            joueurSelectionne?.Vaisseau.RetirerArme(arme);
        }
    }
}
